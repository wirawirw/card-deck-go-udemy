package deck

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {

	d:=NewDeck()

	if len(d)!=16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d))
	}

	if d[0]!="Spades of Ace" {
		t.Errorf("Expected first card in deck is Spades, but got %v", d[0])
	}

	if d[len(d)-1]!="Clubs of Four" {
		t.Errorf("Expected first card in deck is Clubs, but got %v", d[len(d)-1])
	}
}

func TestDeck_SaveFile(t *testing.T) {
	os.Remove("_deckTesting")

	deck:=NewDeck()

	deck.SaveFile("_deckTesting")
	loadedDeck := NewDeckFromFile("_deckTesting")

	if len(loadedDeck) !=16{
		t.Errorf("Expected 16 cards in deck, but got %v",len(loadedDeck))
	}

	os.Remove("_deckTesting")
}
