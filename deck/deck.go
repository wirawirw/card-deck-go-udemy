package deck

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
)

type deck []string

func NewDeck() deck {
	cards:= deck{}

	cardSuit:=[]string{"Spades","Diamonds","Hearts","Clubs"}
	cardValue:=[]string{"Ace","Two","Three","Four"}

	for _, suit := range cardSuit {
		for _, value := range cardValue {
			cards = append(cards,suit+" of "+value)
		}
	}

	return cards
}

func Deal(d deck,handSize int) (deck, deck) {
	return d[handSize:],d[:handSize]
}

func (d deck)Print()  {
	for i,dx := range d {

		fmt.Println(dx,i)
	}
}

func (d deck)ToString() string  {
	return strings.Join(d,",")
}

func (d deck)SaveFile(fileName string) error {
	return ioutil.WriteFile(fileName,[]byte(d.ToString()),0666)
}

func NewDeckFromFile(fileName string) deck {
	bs,err:=ioutil.ReadFile(string(fileName))

	if err!=nil {
		os.Exit(1)
	}

	return deck(strings.Split(string(bs),","))
}

func (d deck)ShuffleDeck() {
	for i := range d {
		newPosition:=rand.Intn(len(d)-1)

		d[i],d[newPosition]=d[newPosition],d[i]
	}
}